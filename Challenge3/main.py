
def nested_object(dict_data,keys):
    for i in keys:
        dict_data= dict_data.get(i)
        if dict_data is None:
            return None
    return dict_data


# object = {"a":{"b":{"c":"d"}}}
# keys = ["a", "b", "c"]

object = {"x":{"y":{"z":"a"}}}
keys = ["x", "y", "z"]

value = nested_object(object,keys)
if value is None:
    print("Keys doesnt match")
else:
    print("value is",value)
