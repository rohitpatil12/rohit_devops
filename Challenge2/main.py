import requests
import json

url = "http://169.254.169.254/latest/meta-data/"


a = requests.request("GET", url)
b={}
b["meta-data"]=a.text.splitlines()
for i,j in enumerate(b["meta-data"]):
    url1=url+j
    c = requests.request("GET",url1)
    if ("/" in j):
        b["meta-data"][i]={j : c.text.splitlines()}
    else:
        b["meta-data"][i]={j : c.text}

json_data=json.dumps(b,indent=3)
with open("meta_data.json","w+") as jd:
    jd.write(json_data)