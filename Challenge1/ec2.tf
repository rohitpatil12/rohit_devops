resource "aws_instance" "ec2" {
    ami =  "ami-005e54dee72cc1d00"
    associate_public_ip_address = "true"
    availability_zone = "ap-southeast-1"
    key_name = aws_key_pair.key.key_name
    root_block_device {
      delete_on_termination = true
      device_name = "/dev/sda1"
      volume_size = 100
      volume_type = "standard"
    }
    subnet_id = aws_subnet.public_subnet.id
    vpc_security_group_ids = [aws_security_group.sg.id]
    user_data = <<-EOF
        #!/bin/bash

        amazon-linux-extras install nginx1 -y
        echo "<h1>welcome</h1>" >  /usr/share/nginx/html/index.html 
        systemctl enable nginx
        systemctl start nginx
        EOF
    tags = {
        Name = "ec2"
    }
}