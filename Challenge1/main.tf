provider "aws" {
  shared_config_files      = ["/Users/rohitpatil/.aws/config"]
  shared_credentials_files = ["/Users/rohitpatil/.aws/credentials"]
  profile                  = "rohit_devops"
  region =  "ap-southeast-1"
}